package com.cangaris.animals;

import com.cangaris.animals.enums.CatsRace;
import com.cangaris.animals.enums.DogsRace;
import com.cangaris.animals.enums.Gender;
import com.cangaris.animals.enums.LionsRace;

public class Main {

    public static void main(String[] args) {

        Cat cat1 = new Cat("Burek", (short) 2, Gender.MALE, CatsRace.BENGAL);
        Cat cat2 = new Cat("Mruczka", (short) 7, Gender.FEMALE, CatsRace.PERSKI);

        System.out.println(cat1);
        System.out.println(cat2);

        Dog dog1 = new Dog("Fafik", (short) 2, Gender.MALE, DogsRace.BULDOG);
        Dog dog2 = new Dog("Kora", (short) 7, Gender.FEMALE, DogsRace.HASKI);

        System.out.println(dog1);
        System.out.println(dog2);

        Lion lion1 = new Lion("Simba", (short) 2, Gender.MALE, LionsRace.AFRYKANSKI, true);
        Lion lion2 = new Lion("Nala", (short) 7, Gender.FEMALE, LionsRace.AMERYKANSKI, false);

        System.out.println(lion1);
        System.out.println(lion2);
    }
}
