package com.cangaris.animals;

import com.cangaris.animals.enums.CatsRace;
import com.cangaris.animals.enums.Gender;
import com.cangaris.animals.enums.LionsRace;

public class Lion extends Cat {

    private Boolean grzywa;
    private LionsRace race;

    public Lion(String name, Short age, Gender gender, LionsRace race, Boolean grzywa) {
        super(name, age, gender);
        this.race = race;
        this.grzywa = grzywa;
    }

    public LionsRace getLionRace() {
        return race;
    }

    public Boolean getGrzywa() {
        return grzywa;
    }

    public void setGrzywa(Boolean grzywa) {
        this.grzywa = grzywa;
    }

    @Override
    public String toString() {
        return "Lion{" +
                "grzywa=" + grzywa +
                ", race=" + race +
                "} " + super.toString();
    }
}
