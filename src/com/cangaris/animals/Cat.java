package com.cangaris.animals;

import com.cangaris.animals.enums.CatsRace;
import com.cangaris.animals.enums.Gender;

public class Cat extends Animal {

    private CatsRace race;

    public Cat(String name, Short age, Gender gender, CatsRace race) {
        super(name, age, gender);
        this.race = race;
    }

    public Cat(String name, Short age, Gender gender) {
        super(name, age, gender);
    }

    public CatsRace getRace() {
        return race;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "race=" + race +
                "} " + super.toString();
    }
}
