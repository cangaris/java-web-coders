package com.cangaris.animals;

import com.cangaris.animals.enums.CatsRace;
import com.cangaris.animals.enums.DogsRace;
import com.cangaris.animals.enums.Gender;

public class Dog extends Animal {

    private DogsRace race;

    public Dog(String name, Short age, Gender gender, DogsRace race) {
        super(name, age, gender);
        this.race = race;
    }

    public DogsRace getRace() {
        return race;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "race=" + race +
                "} " + super.toString();
    }
}
