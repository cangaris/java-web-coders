package com.cangaris.cars;

import com.cangaris.cars.enums.Color;
import com.cangaris.cars.enums.Model;

/**
 * public - z dowolnego miejsca, z dowolnego katalogu mogę dostać się do metody klasy
 * private - tylko z tej samej klasy (z żadnego innego miejsca niż ten sam plik)
 * protected - daje dostę tylko klasom które dziedziczą czyli używają extends
 * package (*** nie ma nic) -> katalog w linux, folder w windows
 */
public class Car {

    private Model model;
    private Color color;

    public Model getModel() {
        return model;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Car(Model model, Color color) {
        this.model = model;
        this.color = color;
    }
}
