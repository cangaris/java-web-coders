package com.cangaris.cars;

import com.cangaris.cars.enums.Color;
import com.cangaris.cars.enums.Model;

public class Jeep extends Car {

    private boolean naped4x4;

    public Jeep(Model model, Color color, boolean naped4x4) {
        super(model, color);
        this.naped4x4 = naped4x4;
    }

    public String czyMa4x4() {
        return this.naped4x4 ? "TAK" : "NIE";
    }
}
