package com.cangaris.cars;

import com.cangaris.cars.enums.Color;
import com.cangaris.cars.enums.Model;

public class Main {

    public static void main(String[] args) {
        // Klasa (plan na dom u architekta) vs Obiekt (konkretnie zbudowany dom  Jana Kowalskiego)
        Car mercedes = new Car(Model.MERCEDES, Color.RED);
        Car bmw = new Car(Model.BMW, Color.BLACK);
        Jeep jeep = new Jeep(Model.JEEP, Color.YELLOW, true);
        bmw.setColor(Color.BLUE);

        Truck truck = new Truck(Model.SCANIA, Color.GREEN, 25);

        System.out.println(mercedes.getModel());
        System.out.println(mercedes.getColor());
        System.out.println("---------");
        System.out.println(bmw.getModel());
        System.out.println(bmw.getColor());
        System.out.println("---------");
        System.out.println(jeep.getModel());
        System.out.println(jeep.getColor());
        System.out.println(jeep.czyMa4x4());
        System.out.println("---------");
        System.out.println(truck.getCapacity());
        System.out.println(truck.getColor());
        System.out.println(truck.getModel());
    }
}
