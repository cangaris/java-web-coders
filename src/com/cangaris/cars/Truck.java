package com.cangaris.cars;

import com.cangaris.cars.enums.Color;
import com.cangaris.cars.enums.Model;

public class Truck extends Car {

    private Integer capacity;

    public Truck(Model model, Color color, Integer capacity) {
        super(model, color);
        this.capacity = capacity;
    }

    public Integer getCapacity() {
        return capacity;
    }
}
