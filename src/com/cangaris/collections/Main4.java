package com.cangaris.collections;

import java.util.List;
import java.util.stream.Collectors;

public class Main4 {

    public static void main(String[] args) {

        List<String> names = List.of(
                "Damian", "Zosia", "Piotr",
                "@67454", "7eer###", "Ania",
                "1", "***K", "Gosia"
        );

        List<String> woman = names.stream()
                .filter(name -> {
                    var code = (int) name.charAt(0); // rzutowanie 1 znaku string na liczbę ASCI
                    return code >= 65 && code <= 90; // znaki od litery A do litery Z
                })
                .collect(Collectors.toList());

        System.out.println(woman);
    }
}
