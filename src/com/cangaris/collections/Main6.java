package com.cangaris.collections;

import com.cangaris.animals.Cat;
import com.cangaris.animals.enums.CatsRace;
import com.cangaris.animals.enums.Gender;

import java.util.List;
import java.util.stream.Collectors;

public class Main6 {

    public static void main(String[] args) {

        Cat cat1 = new Cat("Burek", (short) 2, Gender.MALE, CatsRace.BENGAL);
        Cat cat2 = new Cat("Mruczka", (short) 7, Gender.FEMALE, CatsRace.SYJAMSKI);
        Cat cat3 = new Cat("Mruczka", (short) 3, Gender.FEMALE, CatsRace.PERSKI);
        Cat cat4 = new Cat("Fafik", (short) 12, Gender.MALE, CatsRace.SYJAMSKI);
        Cat cat5 = new Cat("Sonia", (short) 5, Gender.FEMALE, CatsRace.BENGAL);

        List<Cat> cats = List.of(cat1, cat2, cat3, cat4, cat5);

        List<Cat> filtered = cats.stream()
                .filter(cat -> cat.getGender() == Gender.FEMALE)
                .filter(cat -> cat.getAge() > 3)
                .collect(Collectors.toList());

        System.out.println(filtered);
    }
}
