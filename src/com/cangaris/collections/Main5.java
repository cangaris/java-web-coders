package com.cangaris.collections;

import java.util.List;
import java.util.stream.Collectors;

public class Main5 {

    public static void main(String[] args) {

        List<String> names = List.of(
                "Damian", "Zosia", "Piotr",
                "@67454", "7eer###", "Ania",
                "1", "***K", "Gosia"
        );

        List<String> woman = names.stream()
                .filter(name -> name.matches("^[a-zA-Z]+$"))
                .collect(Collectors.toList());

        System.out.println(woman);
    }
}
