package com.cangaris.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Main2 {

    public static void main(String[] args) {

        List<Integer> all = List.of(1,2,3,4,5,6,7,8,9);

        List<Integer> res = all
                .stream() // zamienia all na strumnień na którym można dokonywać operacji nie zmieniając all
                .map(integer -> integer * 2) // każdy element razy 2
                .map(integer -> integer * 3) // potem razy 3
                .filter(integer -> integer > 30) // większy niż 30
                .filter(integer -> integer < 50) // ale mniejszy niż 50
                .limit(2) // weź tylko 2, resztę pomiń
                .collect(Collectors.toList()); // wynik zbierz w całość i zapisz do listy

        System.out.println(res);
    }
}
