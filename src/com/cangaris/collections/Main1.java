package com.cangaris.collections;

import java.util.ArrayList;

public class Main1 {

    public static void main(String[] args) {

        // pętla od zera do 255 rzuca liczbę na znak (char) i dostajemy kod ASCI
        // kod ASCI to numeryczna reprezentacja liter
        for (int i = 0; i < 255; i++) {
            System.out.println( i + " -> "+ (char) i );
        }

        // % - modulo - informacja jaka jest reszta z dzielenia
        // 5 / 2 = reszta 1 (5 % 2 = 1)
        // 6 / 2 = reszta 0 (6 % 2 = 0)
        // Integer (nakładka na małego inta) od int (mały int jest typem prostym)
        // Float od float
        // Short od short
        // Boolean od boolean
        int[] ints = {1,2,3,4,5,6,7,8,9};
        ArrayList<Integer> evens = new ArrayList<>();
        ArrayList<Integer> odd = new ArrayList<>();

        // filtrujemy tablicę na elementy podzielne przez 2 i niepodzielne
        for (int value : ints) {
            if ( value % 2 == 0 ) {
                evens.add(value);
            } else {
                odd.add(value);
            }
        }

        // każdy element jest mnożony razy 2
        for (int i = 0; i < evens.size(); i++) {
            evens.set(i, evens.get(i) * 2);
        }

//        System.out.println(evens);
//        System.out.println(odd);

        //        for (int i = 0; i < ints.length; i++) {
        //            if ( ints[i] % 2 == 0 ) {
        //                System.out.println("Element " + ints[i] + " jest parzysty!");
        //            } else {
        //                System.out.println("Element " + ints[i] + " jest NIEparzysty!");
        //            }
        //        }
    }
}
