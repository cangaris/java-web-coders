package com.cangaris.collections;

import java.util.List;
import java.util.stream.Collectors;

public class Main3 {

    public static void main(String[] args) {

        List<String> names = List.of("Damian", "Ania", "Gosia", "Zosia", "Piotr");

        // String damian = "Damian"; //  char[] = {'D', 'a', 'm', 'i', 'a', 'n'};

        List<String> woman = names.stream()
                // .filter(name -> !name.endsWith("a")) tylko mężczyźni
                // .filter(name -> name.endsWith("a")) // tylko kobiety (imię kończące się na "a")
                // .filter(name -> name.startsWith("A") || name.startsWith("P"))
                .filter(name -> {
                    var code = (int) name.charAt(0); // String jest też tablicą char, odwołać się możemy do index 0 i po rzutowaniu na int mamy kod ASCI
                    return code >= 68 && code <= 71; // jeśli większe równe D lub mniejsze równe G
                })
                .collect(Collectors.toList());

        System.out.println(woman);
    }
}
