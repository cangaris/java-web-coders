package com.cangaris.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // List ->
        // ArrayList - [x][][][][][][][x][][x][][][1][2][3][4][50][6][7][8][]
        // LinkedList - [1(E7)][][x][][][3][x][][4][][x][][5][x][x][6][][x][][][x][7][][8()][]

        // Set ->
        // HashSet
        // TreeSet

        // arr = [1,2,3,4,5];
        // int[] arr = {10,20,30,40,50}; // 5 ele, index 0 dla elementu 1;
        //                for (int i = 0; i < arr.length; i++) {
        //                    System.out.println( arr[i] ); // ???
        //                }
        var arr = new ArrayList<>();
        arr.addAll(List.of(1,2,3,4,4,4,7,8));
        System.out.println(arr.contains(5));
        for (var v : arr) {
            System.out.println(v);
        }


        // Scanner scan = new Scanner(System.in);
        // var od Javy 10 i może być stosowany jako typowanie pola
        // Java sama się domyśli że scan jest typu Sanner!!
        var scan = new Scanner(System.in);

        System.out.println("Co chcesz zrobić?");
        System.out.println("[A] dodaj, [B] odejmij, [C] pomnóż, [D] podziel, [E] pierwiastek(2), [F] potęga");
        String answer = scan.nextLine();

        switch (answer) {
            case "F": {
                run(scan, "F");
                break;
            }
            case "E": {
                run(scan, "E");
                break;
            }
            case "D": {
                run(scan, "D");
                break;
            }
            case "C": {
                run(scan, "C");
                break;
            }
            case "B": {
                run(scan, "B");
                break;
            }
            case "A": {
                run(scan, "A");
                break;
            }
            default:
                System.out.println("Nierozpoznana akcja!");
                main(args);
                break;
        }
    }

    private static void run(Scanner scan, String mode) {
        System.out.println("Wprowadź cyfrę A...");
        String digitA = scan.nextLine();

        System.out.println("Wprowadź cyfrę B...");
        String digitB = scan.nextLine();

        try {
            float a = Float.parseFloat(digitA);
            float b = Float.parseFloat(digitB);
            float result = 0;
            switch (mode) {
                case "A":
                    result = plus(a, b);
                    break;
                case "B":
                    result = minus(a, b);
                    break;
                case "C":
                    result = multiply(a, b);
                    break;
                case "D":
                    result = divide(a, b);
                    break;
                case "E":
                    result = (float) Math.sqrt(a);
                    break;
                case "F":
                    result = (float) Math.pow(a, b);
                    break;
            }
            System.out.println(result);
        } catch (NumberFormatException exception) {
            System.out.println("Twój kod zakończył się błędem rzutowania parametrów...");
        } catch (ArithmeticException exception) {
            System.out.println("Nie można dzielić przez zero!");
        }
    }

    public static float multiply(float a, float b) {
        return a * b;
    }

    public static float divide(float a, float b) {
        return a / b;
    }

    public static float plus(float a, float b) {
        return a + b;
    }

    public static float minus(float a, float b) {
        return a - b;
    }
}
